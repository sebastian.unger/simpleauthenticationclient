package org.ws4d.authentication.test;

/* This is some sample code for a client authenticating with a ws4d:lightbulb device
 * - directly via PIN on commandline(s)
 * - indirectly via flickering and tapping, brokered by
 *   a UI device (e.g. smartphone) (here be magic)
 */

import java.util.Scanner;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.authorization.AuthorizationException;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.dispatch.DefaultServiceReference;
import org.ws4d.java.dispatch.DeviceServiceRegistry;
import org.ws4d.java.incubation.CredentialManagement.SecurityContext;
import org.ws4d.java.incubation.Types.AuthenticationFault;
import org.ws4d.java.incubation.Types.SecurityAlgorithmSet;
import org.ws4d.java.incubation.WSSecurityForDevices.AlgorithmConstants;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.authenticationclient.DefaultAuthenticationClient;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.ClientAuthenticationCallbacks;
import org.ws4d.java.incubation.wspolicy.wspolicyclient.WSSecurityPolicyClient;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

public class SimpleAuthenticationClient extends DefaultClient {

	static {
		AuthenticationEngine.setDefaultOwnerID("urn:uuid:a768a1dd-aff5-40c6-9f7c-89d29d261e6c");
	}

	public static boolean toggleBulb(Device bulbDev) {
		DefaultServiceReference lightBulbServiceReference = (DefaultServiceReference) (bulbDev.getServiceReference(new URI("http://www.ws4d.org/LightBulbService"), SecurityKey.EMPTY_KEY));
		Service lightBulbService = null;
		try {
			lightBulbService = lightBulbServiceReference.getService();
		} catch (CommunicationException e) {
			System.err.println("Error! Could not find Light Bulb Service @ Device " + bulbDev.getEndpointReference().toString());
			return false;
		};

		Operation stateOperation = lightBulbService.getOperation(null, "LightBulbState", null, null);
		Operation switchOperation = lightBulbService.getOperation(null, "LightBulbSwitch", null, null);

		ParameterValue stateIn = stateOperation.createInputValue();
		ParameterValue stateOut = null;
		try {
			stateOut = stateOperation.invoke(stateIn, CredentialInfo.EMPTY_CREDENTIAL_INFO);
		} catch (AuthorizationException e) {
			e.printStackTrace();
			System.err.println("stateOperation: " + e.getMessage());
			return false;
		} catch (InvocationException e) {
			e.printStackTrace();
			System.err.println("stateOperation: " + e.getMessage());
			if (AuthenticationFault.CLIENTNOTAUTHENTICATED.equals(e.getCode()))
				System.err.println("ERROR: Client not authenticated!");
			else if (AuthenticationFault.INTERNALERROR.equals(e.getCode()))
				System.err.println("ERROR: Internal Error!");
			else
				System.err.println("ERROR: Some other invocation exception");
			return false;
		} catch (CommunicationException e) {
			e.printStackTrace();
			System.err.println("stateOperation: " + e.getMessage());
			return false;
		}
		String state = ParameterValueManagement.getString(stateOut, "state");
		String newState;

		if (state.toLowerCase().equals("off"))
			newState = "on";
		else
			newState = "off";

		ParameterValue switchIn = switchOperation.createInputValue();
		ParameterValue switchOut = null;
		ParameterValueManagement.setString(switchIn, "state", newState);
		ParameterValueManagement.setString(switchIn, "id", AuthenticationEngine.getSafeDefaultOwnerID());

		try {
			switchOut = switchOperation.invoke(switchIn, CredentialInfo.EMPTY_CREDENTIAL_INFO);
		} catch (AuthorizationException e) {
			e.printStackTrace();
			System.err.println("switchOperation: " + e.getMessage());
			return false;
		} catch (InvocationException e) {
			e.printStackTrace();
			System.err.println("switchOperation: " + e.getMessage());
			if (AuthenticationFault.CLIENTNOTAUTHENTICATED.equals(e.getCode()))
				System.err.println("ERROR: Client not authenticated!");
			else if (AuthenticationFault.INTERNALERROR.equals(e.getCode()))
				System.err.println("ERROR: Internal Error!");
			else
				System.err.println("ERROR: Some other invocation exception");
			return false;
		} catch (CommunicationException e) {
			e.printStackTrace();
			System.err.println("switchOperation: " + e.getMessage());
			return false;
		}

		System.out.println("Successfully switched " + ParameterValueManagement.getString(switchOut, "state"));

		return true;
	}

	public static void main(String[] args) {
		if (args.length == 0) {
			System.out.println("run java -jar client uuid1 [uuid2]");
			System.out.println("    uuid1 - uuid of device to authenticate with");
			System.out.println("    uuid2 - optional; uuid of device for indirect authentication");
			return;
		}

		ClientAuthenticationCallbacks clPinReader = new ClientAuthenticationCallbacks() {
			@Override
			public int getOOBpinAsInt(QName mechanism) {
				if (new QName(WSSecurityForDevicesConstants.PinAuthentication, WSSecurityForDevicesConstants.NAMESPACE).equals(mechanism)) {
					System.out.print("PIN: ");
					Scanner in = new Scanner(System.in);
					int intOOBss = in.nextInt();
					in.close();
					return intOOBss;
				} else {
					Log.error("Dafuq?!");
					return 0;
				}
			}

			@Override
			public void deliverAuthenticationData(SecurityContext ct) {
				// TODO Auto-generated method stub

			}

			@Override
			public void prereportMechanism(QName mechanism) {
				// TODO Auto-generated method stub

			}

			@Override
			public HashMap receiveAdditionalAuthenticationData() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void provideAdditionalAuthenticationData(Object key,
					Object data) {
				// TODO Auto-generated method stub

			}
		};

		JMEDSFramework.start(null);



		Log.setLogLevel(Log.DEBUG_LEVEL_INFO);

		/* TODO: In a *real* scenario, target as well as uiAuthDev are of course dynamically discovered */
		EndpointReference targetEpr = new EndpointReference(new AttributedURI(args[0]));
		DeviceReference targetDevRef = DeviceServiceRegistry.getDeviceReference(targetEpr, SecurityKey.EMPTY_KEY, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		DeviceReference smartPhoneDevRef = null;
		Boolean indirectAuthentication = false;

		boolean fake = false;

		Device bulbDev = null;
		try {
			bulbDev = targetDevRef.getDevice();
		} catch (CommunicationException e1) {
			System.out.println("Did not find Bulb Device! Was looking at " + targetDevRef.toString() + "\n" + e1.getMessage() + "\nAssuming Fake Device");
			fake = true;
		}

		/* if not in fake mode, try to switch light bulb. If successful, quit. Otherwise authenticate */

		if (!fake) {
			if (toggleBulb(bulbDev)) {
				JMEDSFramework.stop();
				JMEDSFramework.kill();
				return;
			}
		}

		System.out.print("Using ");

		if (args.length == 2) {
			smartPhoneDevRef = WSSecurityPolicyClient.getFirstUIAuthenticatorByMechanism(new QName(WSSecurityForDevicesConstants.PinAuthentication, WSSecurityForDevicesConstants.NAMESPACE));
			if (smartPhoneDevRef != null) {
				indirectAuthentication = true;
				System.out.print("in");
			}
		}

		System.out.println("direct Authentication.");

		QName authenticationType;

		if (indirectAuthentication) {
			authenticationType = new QName(WSSecurityForDevicesConstants.PinAuthentication, WSSecurityForDevicesConstants.NAMESPACE);
		} else {
			authenticationType = new QName(WSSecurityForDevicesConstants.FlickerAuthentication, WSSecurityForDevicesConstants.NAMESPACE);
		}

		AuthenticationEngine ae = AuthenticationEngine.getInstance();
		ae.setIsABroker(true);
		/* add supported OOB mechanisms as well as supported algorithms here. */
		ae.addSupportedOOBauthenticationMechanism(authenticationType);
		/* reordered for testing matching capabilities */
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_AES_CBC128),
						null
						)
				);
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_RC4),
						null
						)
				);
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_AES_CBC128),
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_ENC_AES_CBC128)
						)
				);

		DefaultAuthenticationClient client = new DefaultAuthenticationClient();

		client.setAuthenticationCallbacks(clPinReader);
		int success = client.authenticate(targetDevRef, null);



		if (success == 0 && !fake)
			toggleBulb(bulbDev);

		JMEDSFramework.stop();

		JMEDSFramework.kill();

		return;

	}

}
